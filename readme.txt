=== WP Awesome ===
Contributors: frodeborli
Tags: context menu, content aware menu, usability, right click
Tested up to: 4.6.1
Stable tag: trunk
Requires at least: 4.6

== Introduction ==
WP Awesome is a plugin that tries to make big improvements to WordPress, while
not interfering with the way WordPress works. When you install the plugin, you
will not notice any immediate changes - but these features have been silently
enabled:

== Smart Context Menus ==

Right clicking any element in site preview mode now will render a nice context
menu - that will take you directly to the appropriate admin page for editing.
You (or your customer) will never again have to ask where to find the feature. 
It will save you thousands of mouse clicks.

Pro tip: If you want to see the browser native context menu, simply hold CTRL 
while right clicking.

Pro tip 2: You can extend the context menu by using the filter 
`wpa_context_menu`.

Pro tip 3: Disable the context menus in your wp-config.php file like this:

`define('WPAWESOME_DISABLE_CONTEXT_MENUS', true);`


== Huge uploads ==

By making WordPress upload files in multiple chunks, you can now upload huge 
files through the standard WordPress user interface. No more editing of 
.htaccess files, or php.ini files. The uploaded file will be split into chunks
that are 1.9 MB large. The chunks are stored in the temp folder - until all
chunks have been uploaded. At that time - the plugin will combine all the chunks
into one file again.

Pro tip: You can override the maximum upload size by defining the 
`WPAWESOME_UPLOAD_LIMIT` constant. 

`define('WPAWESOME_UPLOAD_LIMIT', '10000000'); // 10 MB upload limit`

Pro tip 2: Disable the context menus in your wp-config.php file like this:

`define('WPAWESOME_DISABLE_HUGE_UPLOADS', true);`

== 3 * Esc to login ==

Clicking ESC three times, when you're not logged in - will take you directly
to the login page. This way, you won't have to remember the URL of the page that
prompted you to login. You'll be there instantly after login, and then you can
right click the article (or widget or comment) to edit it.

Pro tip: Disable the context menus in your wp-config.php file like this:

`define('WPAWESOME_DISABLE_FAST_LOGIN', true);`

= Features =

* Real context aware menus, with different choices depending on where the user clicks.
* Plugins and themes can extend and alter the menu items by adding filters.
* Works with most themes and plugins, out of the box.
* Actively maintained with clean code.

= Contribute =

The plugin can be easily extended by adding a filter 
`add_filter( 'wpa_context_menu', 'my_context_menu', 10, 2)`. The 
`function my_context_menu` must accept $menu and $items arguments. $items is 
inspected to detect which element the user clicked. You can look at tag names, 
class names, element ids and more. If you want to add menu items, you'll simply 
modify the $menu array structure to add your own choices.

The best way to contribute, is if your module or theme integrates with WP 
Awesome or if you contribute a patch. Patches should fix bugs, improve the user 
experience or add features. Please don't provide patches that make WP Awesome 
support other modules, unless that other module is extremely popular.

There's a [GIT repository and wiki](https://bitbucket.org/wpmotor/wpawesome) too 
if you want to contribute a patch, a translation, provide bug reports or in any 
other way contribute.

= Translators =

The plugin currently has very few translatable strings, but there are a couple.
If you want to contribute with translations there are two ways: 

1. Send a pull request to our bitbucket repository, with your translation file
in the Languages/ folder.

2. Upload your pot-file to  https://bitbucket.org/wpmotor/wpawesome/issues/new 
and mark it as a proposal. We support translation files, but have only a very
few translations available at the moment.


= Recommended Settings =
There are no configuration needed to use this plugin. Just install it, and you are ready.
