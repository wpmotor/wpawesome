<?php
/*
Plugin Name:    WP Awesome by WPmotor.no
Plugin URI:     http://plugins.wpmotor.no/
Description:    Removes upload size limitation, adds context menus and makes login faster.
Version:        1.0
Author:         Frode Børli
License:        GPLv2
Domain Path:    Languages
Text Domain:    wpa
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Define the auto loader
 */
spl_autoload_register( function($className) {
    if(substr($className, 0, 4) != 'WPA\\') {
        return;
    }
    
	if(substr($className, 0, 11) == 'WPA\\Module\\') {
		require( __DIR__ . '/modules/' . str_replace('\\', '/', substr($className, 11)) . '.php' );
	} else {
	    require( __DIR__ . '/inc/' . str_replace('\\', '/', substr($className, 4)) . '.php');
	}
	
} );

function get_attrs($item=null) {
	if($item === null) {
		$item = get_post();
	}
	return new Attrs($item);
}

function get_menu_items($location) {

	$locations = get_nav_menu_locations();
	if(!isset($locations[$location]))
		return false;

	$menu = wp_get_nav_menu_object($locations[$location]);
	return wp_get_nav_menu_items($menu->term_id, array( 'update_post_term_cache' => false ));
}

define( 'WPA_URL', plugin_dir_url( __FILE__ ));
define( 'WPA_ROOT', __DIR__ );
\WPA\Awesome::load();
